import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'question', loadChildren: './question/question.module#QuestionPageModule' },
  { path: 'results/:duration/:questionCounter/:correctAnsCounter', loadChildren: './results/results.module#ResultsPageModule' },
  { path: 'answers', loadChildren: './answers/answers.module#AnswersPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
