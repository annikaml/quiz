import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AnswersPage } from '../answers/answers.page';

@Component({
  selector: 'app-results',
  templateUrl: './results.page.html',
  styleUrls: ['./results.page.scss'],
})
export class ResultsPage implements OnInit {

  duration: number;
  durationSeconds: number;
  correctAnsCounter: number;
  questionCounter: number;
  scorePercentage: number;
  feedback: string;
  feedbackPhoto: string;

  constructor(public router: Router, public activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.duration = Number(this.activatedRoute.snapshot.paramMap.get('duration'));
    this.durationSeconds = Math.round((this.duration) / 1000);
    
    this.questionCounter = Number(this.activatedRoute.snapshot.paramMap.get('questionCounter'));
    this.correctAnsCounter = Number(this.activatedRoute.snapshot.paramMap.get('correctAnsCounter'));
    this.scorePercentage = Math.round(this.correctAnsCounter / this.questionCounter * 100);

    if (this.correctAnsCounter <= 1) {
        this.feedback = 'Yikes! That didn\'t go too well!';
        this.feedbackPhoto = '../../assets/imgs/bad.jpg';
    } else if (this.correctAnsCounter <= 2) {
        this.feedback = 'Pretty good!';
        this.feedbackPhoto = '../../assets/imgs/prettygood.jpg';
    } else if (this.correctAnsCounter === 3) {
        this.feedback = 'Good job! You got almost all correct';
        this.feedbackPhoto = '../../assets/imgs/goodjob.jpg';
    } else if (this.correctAnsCounter === 4 && this.durationSeconds > 15) {
        this.feedback = 'Nailed it! Try to improve your time next!';
        this.feedbackPhoto = '../../assets/imgs/nailedit.png';
    } else if (this.correctAnsCounter === 4 && this.durationSeconds <= 10) {
        this.feedback = 'Amazing! You were fast as lightning!';
        this.feedbackPhoto = '../../assets/imgs/amazing.jpg';
    }
  }

  answers() {
    this.router.navigateByUrl('answers')
  }
}


