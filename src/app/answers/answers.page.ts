import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-answers',
  templateUrl: './answers.page.html',
  styleUrls: ['./answers.page.scss'],
})
export class AnswersPage implements OnInit {

  quests: any;

  constructor( public router: Router, public activatedRoute: ActivatedRoute ) { }

  ngOnInit() {
    fetch('../../assets/data/quests.json').then(res => res.json())
    .then(json => {
      this.quests = json;
       });
  }

  end() {
    this.router.navigateByUrl('home');
  }

}
