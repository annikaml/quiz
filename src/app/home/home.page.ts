import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { QuestionPage } from '../question/question.page';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(public router: Router) {}

  ngOnInit() {
  }

  question() {
    this.router.navigateByUrl('question')
  }

}
