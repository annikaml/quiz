import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ResultsPage } from '../results/results.page';
import { Question } from '../../question';

@Component({
  selector: 'app-question',
  templateUrl: './question.page.html',
  styleUrls: ['./question.page.scss'],
})

export class QuestionPage implements OnInit {
  
  quests: Question[] = [];
  activeQuestion: Question;
  isAnswered: boolean;
  isCorrect: boolean;
  feedback: string;
  questionCounter: number;
  optionCounter: number;
  correctAnsCounter: number;
  startTime: Date;
  endTime: Date;
  duration: number;

  constructor(public router: Router, public activatedRoute: ActivatedRoute) {}

  


  ngOnInit() {
    this.questionCounter = 0;
    this.correctAnsCounter = 0;
    this.startTime = new Date();
    fetch('../../assets/data/quests.json').then(res => res.json())
    .then(json => {
      this.quests = json;
      console.log(this.quests.length);
      this.setQuestion();
       });
       
  }

   setQuestion() {
    if (this.questionCounter === this.quests.length) {
      this.endTime = new Date();
      this.duration = this.endTime.getTime() - this.startTime.getTime();
      this.router.navigateByUrl('results/' + this.duration + '/' + this.questionCounter + '/' + this.correctAnsCounter);
      this.ngOnInit();
    } else {
    this.isAnswered = false;
    this.feedback = '';
    this.isCorrect = false;
    this.activeQuestion = this.quests[this.questionCounter];
    this.questionCounter++;
    }
  }

  checkOption(option: number, activeQuestion: Question) {
    if (option === activeQuestion.correctOption) {
      this.isCorrect = true;
      this.feedback = activeQuestion.options[option] + 
      ' is correct! Go to the next question.';
      this.correctAnsCounter++;
      this.isAnswered = true;
    } else {
      this.isCorrect = false;
      this.feedback = 'Incorrect. Go to the next question.';
      this.isAnswered = true;
    }
  }
 
}
